package com.finfabrik;

import com.finfabrik.persistence.*;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@MicronautTest
public class HibernateTests {
    @Inject
    private UserRepository userRepository;

    @Test
    public void testSave() {
        var emailIdentity = new EmailIdentity();
        emailIdentity.setEmail("a@b.com");

        var linkedinIdentity = new LinkedinIdentity();
        linkedinIdentity.setUrl("https://linkedin.com");

        var profile = new EmployeeProfile();
        profile.setType((short) 1);
        profile.setAddress("address");
        profile.setFname("fname");
        profile.setLname("lname");

        var user = new User();
        user.setName("testUser");
        user.setIdentityList(Arrays.asList(new BaseUserIdentity[] {emailIdentity, linkedinIdentity}.clone()));
        user.setUserProfile(profile);

        var saved = userRepository.save(user);

        assertNotNull(saved);
        assertNotNull(saved.getId());
        assertEquals("testUser", saved.getName());
        assertEquals(2, saved.getIdentityList().size());

        assertNotNull(saved.getUserProfile());
        assertEquals((short) 1, saved.getUserProfile().getType());
        assertEquals("address", ((EmployeeProfile) saved.getUserProfile()).getAddress());
        assertEquals("fname", ((EmployeeProfile) saved.getUserProfile()).getFname());
        assertEquals("lname", ((EmployeeProfile) saved.getUserProfile()).getLname());
    }

    @Test
    public void testGet() {
        var emailIdentity = new EmailIdentity();
        emailIdentity.setEmail("a@b.com");

        var linkedinIdentity = new LinkedinIdentity();
        linkedinIdentity.setUrl("https://linkedin.com");

        var profile = new EmployeeProfile();
        profile.setType((short) 1);
        profile.setAddress("address");
        profile.setFname("fname");
        profile.setLname("lname");

        var user = new User();
        user.setName("testUser");
        user.setIdentityList(Arrays.asList(new BaseUserIdentity[] {emailIdentity, linkedinIdentity}.clone()));
        user.setUserProfile(profile);

        var saved = userRepository.save(user);

        // test

        var found = userRepository.findById(saved.getId()).orElseThrow();

        assertNotNull(found);
        assertNotNull(found.getId());
        assertEquals("testUser", found.getName());
        assertEquals(2, found.getIdentityList().size());

        assertNotNull(found.getUserProfile());
        assertEquals((short) 1, found.getUserProfile().getType());
        assertEquals("address", ((EmployeeProfile) found.getUserProfile()).getAddress());
        assertEquals("fname", ((EmployeeProfile) found.getUserProfile()).getFname());
        assertEquals("lname", ((EmployeeProfile) found.getUserProfile()).getLname());
    }
}
