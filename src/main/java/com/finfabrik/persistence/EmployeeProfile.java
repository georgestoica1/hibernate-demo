package com.finfabrik.persistence;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "employee_profile")
public class EmployeeProfile extends BaseUserProfile {
    @Column(name = "first_name")
    private String fname;
    @Column(name = "last_name")
    private String lname;
    @Column
    private String address;
}
