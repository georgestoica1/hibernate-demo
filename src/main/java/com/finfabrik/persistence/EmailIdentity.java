package com.finfabrik.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;

@NoArgsConstructor
@Data
@Entity(name = "email_identity")
public class EmailIdentity extends BaseUserIdentity {
    @Column
    private String email;
}
