package com.finfabrik.persistence;

import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "profile_assoc")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class BaseUserProfile {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private Short type;

    @Column
    private Long userId;
}
