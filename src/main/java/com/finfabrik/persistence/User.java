package com.finfabrik.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@Data
@Entity(name = "user")
public class User {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @OneToMany(mappedBy = "userId", cascade = CascadeType.PERSIST)
    private List<BaseUserIdentity> identityList;

    @OneToOne(cascade = CascadeType.PERSIST)
    private BaseUserProfile userProfile;
}
