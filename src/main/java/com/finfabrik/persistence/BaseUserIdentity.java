package com.finfabrik.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@Data
@Entity(name = "identity_assoc")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class BaseUserIdentity {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private Short type;

    @Column
    private Long userId;
}
