package com.finfabrik.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;

@NoArgsConstructor
@Data
@Entity(name = "linkedin_identity")
public class LinkedinIdentity extends BaseUserIdentity {
    @Column
    private String url;
}
